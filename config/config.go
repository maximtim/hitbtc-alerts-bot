package config

import (
	"sigbot/hitbtc_bot/lib/util/env"
	"time"
)

type Config struct {
	RestURL         string
	SocketURL       string
	DialTimeout     time.Duration
	ResponseTimeout time.Duration
	RateLimit       int

	BuffSize       int
	CacheFileName  string
	CacheFlushFreq int

	NPlotWorkers int
	PlotDir      string
}

func New() *Config {
	return &Config{
		RestURL:         env.GetString("REST_URL", "https://api.hitbtc.com/api/3/public"),
		SocketURL:       env.GetString("SOCKET_URL", "wss://api.hitbtc.com/api/3/ws/public"),
		DialTimeout:     env.GetDuration("DIAL_TIMEOUT", 10*time.Second),
		ResponseTimeout: env.GetDuration("RESP_TIMEOUT", 10*time.Second),
		RateLimit:       int(env.GetInt("RATE_LIMIT", 20)),

		BuffSize:       int(env.GetInt("BUFF_SIZE", 100)),
		CacheFileName:  env.GetString("DB_FILE", "trades.sqlite"),
		CacheFlushFreq: int(env.GetInt("FLUSH_FREQ", 100)),

		NPlotWorkers: int(env.GetInt("N_WORKERS", 4)),
		PlotDir:      env.GetString("PLOT_DIR", "plots"),
	}
}
