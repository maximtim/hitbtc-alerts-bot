from sys import stdin
import bars
from contextlib import closing
import sqlite3
from sys import stderr
import matplotlib as mpl

if __name__ == '__main__':
    mpl.use('agg')
    
    db_file = '../trades.sqlite'
    conn = sqlite3.connect(db_file)

    with closing(conn) as db:
        for line in stdin:
            exec(line)
