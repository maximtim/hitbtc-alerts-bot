import common
import mplfinance as mpf
from datetime import datetime

def plot_bars(db, symbol :str, resolution: str, end :str, count :int, out :str):
    end_dt = datetime.strptime(end, '%Y-%m-%d %H:%M:%S+00:00')
    trades = common.load_trades(db, symbol, resolution, count, end_dt)
    bars = common.build_bars(trades, resolution, count, end_dt)
    mpf.plot(
        bars, type='candle', volume=True, datetime_format='%H:%M:%S',
        ylabel="", ylabel_lower='', savefig=out, figratio=(18, 10),closefig=True
    )
    print(out)