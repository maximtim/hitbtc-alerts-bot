from contextlib import closing
import pandas as pd
import numpy as np
from datetime import datetime


def load_trades(db, symbol: str, resolution: str, count_bars: int, end :datetime) -> pd.DataFrame:
    """Load trades from db_file in given time range."""

    select_sql = f'''
    SELECT time, price, qty FROM "{symbol}"
    WHERE time >= ? AND time <= ?
    '''
    start_time = end - count_bars * pd.Timedelta(resolution).to_pytimedelta()
    df = pd.read_sql(select_sql, db, params=(start_time.isoformat(sep=' '), end.isoformat(sep=' ')))
    df["time"] = pd.to_datetime(df["time"])

    return df


def build_bars(trades: pd.DataFrame, resolution: str, count: int, end :datetime) -> pd.DataFrame:
    time_index = pd.date_range(end=end, periods=count, freq=resolution, tz='UTC').floor(resolution)
    bars = pd.DataFrame(
        index=time_index,
        columns=['Open', 'High', 'Low', 'Close', 'Volume']
    )

    if len(trades) == 0:
        bars.fillna(0, inplace=True)
        return bars
    start = trades.iloc[0].time.ceil(resolution)
    data = trades[trades.time >= start]

    grouped = data.groupby(lambda idx: data.loc[idx].time.floor(resolution))
    vals = grouped.agg([np.max, np.min, 'last', np.sum])
    open_vals = np.insert(vals.price['last'].array[:-1], 0, data.iloc[0].price)

    bars.loc[vals.index] = pd.DataFrame({
        'Open': open_vals,
        'High': np.maximum(vals.price['amax'].array, open_vals),
        'Low': np.minimum(vals.price['amin'].array, open_vals),
        'Close': vals.price['last'],
        'Volume': vals.qty['sum'],
    })

    bars.Close.fillna(method='ffill', inplace=True)
    bars.Open.fillna(method='bfill', inplace=True)
    bars.Close.fillna(bars.Open, inplace=True)
    bars.Open.fillna(bars.Close, inplace=True)
    bars.High.fillna(bars.Close, inplace=True)
    bars.Low.fillna(bars.Close, inplace=True)
    bars.Volume.fillna(0, inplace=True)
    
    return bars


def time_type(value):
    return datetime.strptime(value, "%Y-%m-%dT%H:%M:%S")