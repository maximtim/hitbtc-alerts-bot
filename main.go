package main

import (
	"fmt"
	"sigbot/hitbtc_bot/config"
	"sigbot/hitbtc_bot/lib/logging"
	"sigbot/hitbtc_bot/lib/plotter"
	"time"
)

func main() {
	cfg := config.New()
	// service, err := service.New(cfg)
	// if err != nil {
	// 	logging.Error.Fatalf("failed to create service, err=%v", err)
	// }

	// symbols, err := service.Symbols()
	// if err != nil {
	// 	logging.Error.Fatalf("failed to get symbols, err=%v", err)
	// }
	// service.Subscribe(symbols)
	// service.Serve()

	// stress testing
	p, err := plotter.New(cfg)
	if err != nil {
		logging.Error.Fatalf("failed to create plotter, err=%v", err)
	}
	if err = p.Start(); err != nil {
		logging.Error.Fatalf("failed to start plotter, err=%v", err)
	}

	picCount := 1000
	for i := 0; i < picCount; i++ {
		p.PlotBars("BTCUSDT", time.Minute, time.Date(2021, 12, 10, 15, 0, 0, 0, time.UTC), 60, fmt.Sprintf("%d", i))
	}

	for i := 0; i < picCount; i++ {
		if err := <-p.Ch; err != nil {
			logging.Errorf("failed to plot bars, err=%v", err)
		}
	}
	p.Stop()
	logging.Infof("plotted bars")
}
