package service

import (
	"sigbot/hitbtc_bot/config"
	"sigbot/hitbtc_bot/lib/database"
	"sigbot/hitbtc_bot/lib/logging"
	"sigbot/hitbtc_bot/service/api/rest"
	"sigbot/hitbtc_bot/service/api/streaming"
	"time"

	"golang.org/x/time/rate"
)

type Service struct {
	cfg       *config.Config
	api       *rest.API
	streaming *streaming.Streaming
	db        *database.DataBase
}

func New(cfg *config.Config) (*Service, error) {
	db, err := database.New(cfg)
	if err != nil {
		return nil, err
	}

	rl := rate.NewLimiter(rate.Every(time.Second), cfg.RateLimit)

	a := rest.New(cfg, rl)
	s := streaming.New(cfg, rl)

	return &Service{
		cfg:       cfg,
		api:       a,
		streaming: s,
		db:        db,
	}, nil
}

func (s *Service) Serve() {
	s.streaming.Start()
	for trade := range s.streaming.Channel() {
		logging.Infof("got trade %+v", trade)
		s.db.Add(trade)
	}
}

func (s *Service) Symbols() ([]string, error) {
	return s.api.Symbols()
}

func (s *Service) Subscribe(symbols []string) {
	s.streaming.Subscribe(symbols)
}
