package streaming

import (
	"encoding/json"
	"fmt"
	"sigbot/hitbtc_bot/lib/types"
	"strconv"
	"time"
)

type request struct {
	Method  string                 `json:"method"`
	Channel string                 `json:"ch"`
	Params  map[string]interface{} `json:"params"`
	ID      string                 `json:"id"`
}

func (r *request) GetSymbols() []string {
	return r.Params["symbols"].([]string)
}

type result struct {
	Channel       string   `json:"ch"`
	Subscriptions []string `json:"subscriptions"`
}

type response struct {
	Res result `json:"result"`
	ID  string `json:"id"`
}

type trade struct {
	Timestamp int64  `json:"t"`
	ID        int64  `json:"i"`
	Price     string `json:"p"`
	Qty       string `json:"q"`
	Side      string `json:"s"`
}

func newTrade(symbol string, t *trade) (*types.Trade, error) {
	ts := time.Unix(0, t.Timestamp*1000*1000).UTC()
	price, err := strconv.ParseFloat(t.Price, 64)
	if err != nil {
		return nil, err
	}
	qty, err := strconv.ParseFloat(t.Qty, 64)
	if err != nil {
		return nil, err
	}
	if t.Side != types.SideBuy && t.Side != types.SideSell {
		return nil, fmt.Errorf("unknown side string: %q", t.Side)
	}

	return &types.Trade{
		Symbol:    symbol,
		Timestamp: ts,
		ID:        t.ID,
		Price:     price,
		Qty:       qty,
		Side:      t.Side,
	}, nil
}

type notification struct {
	Channel  string              `json:"ch"`
	Snapshot map[string][]*trade `json:"snapshot,omitempty"`
	Update   map[string][]*trade `json:"update,omitempty"`
}

func (n *notification) IsSnapshot() bool {
	return n.Snapshot != nil
}

func (n *notification) IsUpdate() bool {
	return n.Update != nil
}

func UnmarshalMsg(data []byte) (msg interface{}, err error) {
	msg = &response{}
	if err = json.Unmarshal(data, msg); err == nil && msg.(*response).ID != "" {
		return
	}

	msg = &notification{}
	if err = json.Unmarshal(data, msg); err == nil {
		return
	}

	return
}
