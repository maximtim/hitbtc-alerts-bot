package streaming

import (
	"context"
	"encoding/json"
	"net/http"
	"sigbot/hitbtc_bot/config"
	"sigbot/hitbtc_bot/lib/logging"
	"sigbot/hitbtc_bot/lib/types"
	"sync"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"golang.org/x/time/rate"
)

const (
	subscribeMethod   = "subscribe"
	unsubscribeMethod = "unsubscribe"

	tradesChannel = "trades"
)

type Streaming struct {
	conn         *websocket.Conn
	cfg          *config.Config
	awaitingReqs *sync.Map
	out          chan *types.Trade
	rl           *rate.Limiter
}

func New(cfg *config.Config, rl *rate.Limiter) *Streaming {
	dialer := &websocket.Dialer{
		Proxy:            http.ProxyFromEnvironment,
		HandshakeTimeout: cfg.DialTimeout,
	}
	conn, rsp, err := dialer.Dial(cfg.SocketURL, http.Header{})
	if err != nil {
		logging.Error.Fatalf("failed dialing %q, err=%v", cfg.SocketURL, err)
	}
	if rsp.StatusCode != http.StatusSwitchingProtocols {
		logging.Error.Fatalf("failed dialing %q, err=%v", cfg.SocketURL, err)
	}

	r := &Streaming{
		conn:         conn,
		cfg:          cfg,
		awaitingReqs: &sync.Map{},
		out:          make(chan *types.Trade, cfg.BuffSize),
		rl:           rl,
	}
	return r
}

func (s *Streaming) Start() {
	go s.readerLoop()
}

func (s *Streaming) Stop() {
	logging.Infof("stopping streaming")
	close(s.out)
}

func (s *Streaming) Channel() chan *types.Trade {
	return s.out
}

func (s *Streaming) readerLoop() {
	for {
		_, data, err := s.conn.ReadMessage()
		if err != nil {
			logging.Warnf("failed to read message")
			continue
		}

		msg, err := UnmarshalMsg(data)
		if err != nil {
			logging.Warnf("failed to parse message %q, err=%v", string(data), err)
			continue
		}
		s.handleMessage(msg)
	}
}

func (s *Streaming) Subscribe(symbols []string) {
	req := &request{
		Method:  subscribeMethod,
		Channel: tradesChannel,
		Params: map[string]interface{}{
			"symbols": symbols,
		},
		ID: uuid.New().String(),
	}
	s.sendRequest(req)
}

func (s *Streaming) sendRequest(req *request) {
	if err := s.rl.Wait(context.Background()); err != nil {
		logging.Warnf("failed to wait on rate limit, err=%v", err)
	}

	reqData, err := json.Marshal(req)
	if err != nil {
		logging.Warnf("failed to serialize %s request %s, err=%v", req.Method, req.ID, err)
		return
	}

	if err := s.conn.WriteMessage(websocket.TextMessage, reqData); err != nil {
		logging.Warnf("failed to send %s request %s, err=%v", req.Method, req.ID, err)
	}

	s.awaitingReqs.Store(req.ID, req.GetSymbols())
	go func() {
		<-time.After(s.cfg.ResponseTimeout)
		if _, ok := s.awaitingReqs.LoadAndDelete(req.ID); ok {
			logging.Warnf("response waiting timeout on %s request %s", req.Method, req.ID)
		}
	}()
}

func (s *Streaming) handleMessage(msg interface{}) {
	switch typ := msg.(type) {
	case *response:
		s.handleResponse(msg.(*response))
		break
	case *notification:
		s.handleNotification(msg.(*notification))
		break
	default:
		logging.Warnf("unexpected message type %T", typ)
	}

}

func (s *Streaming) handleResponse(rsp *response) {
	if _, ok := s.awaitingReqs.LoadAndDelete(rsp.ID); !ok {
		logging.Warnf("got response for unexpected request %s", rsp.ID)
	} else {
		logging.Infof("request %s successfully executed, subscribed to %d symbols", rsp.ID, len(rsp.Res.Subscriptions))
	}
}

func (s *Streaming) handleNotification(n *notification) {
	symbols := make([]string, 0, len(n.Snapshot))
	tradesCnt := 0
	if n.IsSnapshot() {
		for symbol, trades := range n.Snapshot {
			for i := range trades {
				go s.processTrade(symbol, trades[i])
			}
			symbols = append(symbols, symbol)
			tradesCnt += len(trades)
		}
	}
	if n.IsUpdate() {
		for symbol, trades := range n.Update {
			for i := range trades {
				go s.processTrade(symbol, trades[i])
			}
			symbols = append(symbols, symbol)
			tradesCnt += len(trades)
		}
	}
	logging.Infof("processed notification for symbols %v of %d trades", symbols, tradesCnt)
}

func (s *Streaming) processTrade(symbol string, t *trade) {
	trade, err := newTrade(symbol, t)
	if err != nil {
		logging.Warnf("failed to parse trade id=%d, skipping", trade.ID)
		return
	}

	select {
	case s.out <- trade:
	default:
		logging.Warnf("dropping trade for %s, buffer is full", symbol)
	}
}
