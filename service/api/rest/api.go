package rest

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"sigbot/hitbtc_bot/config"

	"golang.org/x/time/rate"
)

type API struct {
	cfg    *config.Config
	client http.Client
	rl     *rate.Limiter
}

func New(cfg *config.Config, rl *rate.Limiter) *API {
	return &API{
		cfg:    cfg,
		client: *http.DefaultClient,
		rl:     rl,
	}
}

func (a *API) Symbols() ([]string, error) {
	if err := a.rl.Wait(context.Background()); err != nil {
		return nil, err
	}

	req, err := http.NewRequest(http.MethodGet, a.cfg.RestURL+"/symbol", nil)
	if err != nil {
		return nil, err
	}

	rsp, err := a.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer rsp.Body.Close()

	body, err := ioutil.ReadAll(rsp.Body)
	if err != nil {
		return nil, err
	}

	symRsp := &response{}
	if err := json.Unmarshal(body, symRsp); err != nil {
		return nil, err
	}

	symbols := make([]string, 0, len(*symRsp))
	for symbol := range *symRsp {
		symbols = append(symbols, symbol)
	}
	return symbols, nil
}
