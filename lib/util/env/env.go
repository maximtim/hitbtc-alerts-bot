package env

import (
	"os"
	"strconv"
	"time"
)

func GetString(key, defVal string) string {
	val := os.Getenv(key)
	if val == "" {
		return defVal
	}
	return val
}

func GetInt(key string, defVal int64) int64 {
	val := os.Getenv(key)
	if val == "" {
		return defVal
	}
	iVal, err := strconv.ParseInt(val, 10, 64)
	if err != nil {
		return defVal
	}
	return iVal
}

func GetDuration(key string, defVal time.Duration) time.Duration {
	val := os.Getenv(key)
	if val == "" {
		return defVal
	}
	dVal, err := time.ParseDuration(val)
	if err != nil {
		return defVal
	}
	return dVal
}

func GetBool(key string) bool {
	val := os.Getenv(key)
	return val != ""
}
