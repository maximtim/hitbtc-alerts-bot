package env

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestGetInt(t *testing.T) {
	os.Setenv("key", "5")

	i := GetInt("key", 3)
	j := GetInt("gay", 4)
	require.Equal(t, int64(5), i)
	require.Equal(t, int64(4), j)
}
