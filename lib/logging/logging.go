package logging

import (
	"log"
	"os"
)

var (
	Info  = log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	Warn  = log.New(os.Stdout, "WARN\t", log.Ltime|log.Ldate)
	Error = log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)
)

func Infof(format string, v ...interface{}) {
	Info.Printf(format, v...)
}

func Warnf(format string, v ...interface{}) {
	Warn.Printf(format, v...)
}

func Errorf(format string, v ...interface{}) {
	Error.Printf(format, v...)
}
