package database

import (
	"database/sql"
	"errors"
	"fmt"
	"sigbot/hitbtc_bot/config"
	"sigbot/hitbtc_bot/lib/logging"
	"sigbot/hitbtc_bot/lib/types"

	_ "github.com/mattn/go-sqlite3"
)

const (
	createSymbolSchemaTemplate = `
		CREATE TABLE IF NOT EXISTS %q (
			id    INTEGER,
			time  TIMESTAMP,
			price DOUBLE,
			qty   DOUBLE,
			side  TEXT
		);
	`

	insertBarQueryTemplate = `
		INSERT INTO %q (
			id, time, price, qty, side
		) VALUES (
			?, ?, ?, ?, ?
		)
	`
)

// DataBase is a database of stock bars.
type DataBase struct {
	sql    *sql.DB
	stmts  map[string]*sql.Stmt
	buffer []*types.Trade
}

// New constructs a Trades value for managing stock trades in a
// SQLite database. This API is not thread safe.
func New(cfg *config.Config) (*DataBase, error) {
	sqlDB, err := sql.Open("sqlite3", cfg.CacheFileName)
	if err != nil {
		return nil, err
	}

	db := DataBase{
		sql:    sqlDB,
		stmts:  map[string]*sql.Stmt{},
		buffer: make([]*types.Trade, 0, cfg.CacheFlushFreq),
	}
	return &db, nil
}

func (db *DataBase) CreateSymbol(symbol string) error {
	schemaSQL := fmt.Sprintf(createSymbolSchemaTemplate, symbol)
	logging.Infof("exequting query %q", schemaSQL)
	res, err := db.sql.Exec(schemaSQL)
	if err != nil {
		logging.Errorf("failed to execute query, err=%v", err)
		return err
	}

	lastID, err := res.LastInsertId()
	if err != nil {
		logging.Errorf("failed to execute query, err=%v", err)
		return err
	}

	insertSQL := fmt.Sprintf(insertBarQueryTemplate, symbol)
	stmt, err := db.sql.Prepare(insertSQL)
	if err != nil {
		logging.Errorf("failed to prepare stattement, query=%q", insertSQL)
		return err
	}
	db.stmts[symbol] = stmt

	logging.Infof("successfully created symbol %s, id=%v", symbol, lastID)
	return nil
}

func (d *DataBase) HasSymbol(symbol string) bool {
	_, ok := d.stmts[symbol]
	return ok
}

// Add stores a bar into the buffer. Once the buffer is full, the
// bars are flushed to the database.
func (db *DataBase) Add(trade *types.Trade) error {
	if !db.HasSymbol(trade.Symbol) {
		db.CreateSymbol(trade.Symbol)
	}

	if len(db.buffer) == cap(db.buffer) {
		return errors.New("trades buffer is full")
	}

	db.buffer = append(db.buffer, trade)
	if len(db.buffer) == cap(db.buffer) {
		if err := db.Flush(); err != nil {
			return fmt.Errorf("unable to flush trades: %w", err)
		}
	}
	return nil
}

// Flush inserts pending bars into the database.
func (db *DataBase) Flush() error {
	tx, err := db.sql.Begin()
	if err != nil {
		return err
	}

	for _, trade := range db.buffer {
		if !db.HasSymbol(trade.Symbol) {
			db.CreateSymbol(trade.Symbol)
		}

		_, err := tx.Stmt(db.stmts[trade.Symbol]).Exec(trade.ID, trade.Timestamp, trade.Price, trade.Qty, trade.Side)
		if err != nil {
			tx.Rollback()
			return err
		}
	}

	db.buffer = db.buffer[:0]
	if err := tx.Commit(); err != nil {
		return err
	}
	logging.Infof("successfully flushed %d trades to local cache", cap(db.buffer))
	return nil
}

// Close flushes all trades to the database and prevents any future trading.
func (db *DataBase) Close() error {
	defer func() {
		for _, stmt := range db.stmts {
			stmt.Close()
		}
		db.sql.Close()
	}()

	if err := db.Flush(); err != nil {
		return err
	}

	return nil
}
