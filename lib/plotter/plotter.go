package plotter

import (
	"fmt"
	"math/rand"
	"os"
	"sigbot/hitbtc_bot/config"
	"sigbot/hitbtc_bot/lib/logging"
	"sync"
	"time"
)

type Plotter struct {
	workers []*worker
	mux     []*sync.Mutex
	Ch      chan error
}

func New(cfg *config.Config) (p *Plotter, err error) {
	p = &Plotter{
		workers: make([]*worker, cfg.NPlotWorkers),
		mux:     make([]*sync.Mutex, cfg.NPlotWorkers),
		Ch:      make(chan error, cfg.NPlotWorkers),
	}
	for i := range p.workers {
		p.mux[i] = &sync.Mutex{}
	}

	if err := os.MkdirAll(fmt.Sprintf("scripts/%s", cfg.PlotDir), 0777); err != nil {
		return nil, err
	}
	for i := range p.workers {
		if p.workers[i], err = newWorker(cfg.PlotDir, p.Ch); err != nil {
			return
		}
	}
	return
}

func (p *Plotter) Start() error {
	for i, worker := range p.workers {
		if err := worker.start(); err != nil {
			return err
		}
		logging.Infof("plot worker %d started successfully", i)

	}
	return nil
}

func (p *Plotter) Stop() {
	for _, worker := range p.workers {
		worker.stop()
	}
	close(p.Ch)
}

func (p *Plotter) PlotBars(symbol string, resolution time.Duration, end time.Time, count int, id string) {
	i := rand.Intn(len(p.workers))
	p.workers[i].tasks <- func(w *worker) {
		logging.Infof("worker %d plotting bars, plot id: %s", i, id)
		savedPath, err := w.plotBars(symbol, resolution, end, count, id)
		if err == nil {
			logging.Infof("worker %d saved plot %s as %s", i, id, savedPath)
		}
		w.out <- err
	}
}
