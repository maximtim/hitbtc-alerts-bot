package plotter

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os/exec"
	"sigbot/hitbtc_bot/lib/logging"
	"time"
)

const (
	startupPath      = "scripts/startup.py"
	plotBarsTemplate = "bars.plot_bars(db, %q, \"%ds\", %q, %d, %q)\n"
)

type worker struct {
	cmd    *exec.Cmd
	stdin  io.WriteCloser
	stdout io.ReadCloser
	stderr io.ReadCloser

	tasks   chan func(*worker)
	plotDir string
	out     chan error
}

func newWorker(plotDir string, out chan error) (w *worker, err error) {
	cmd := exec.Command("/usr/bin/python3", "-u", "interpreter.py")
	cmd.Dir = "scripts"

	w = &worker{
		cmd:     cmd,
		tasks:   make(chan func(*worker), 1000),
		plotDir: plotDir,
		out:     out,
	}
	w.stdin, err = cmd.StdinPipe()
	if err != nil {
		return
	}
	w.stdout, err = cmd.StdoutPipe()
	if err != nil {
		return
	}
	w.stderr, err = cmd.StderrPipe()
	return
}

func (w *worker) start() error {
	if err := w.cmd.Start(); err != nil {
		return err
	}

	go w.readLoop(w.stderr, logging.Warn)
	go w.execLoop()
	return nil
}

func (w *worker) readLoop(rc io.ReadCloser, log *log.Logger) {
	r := bufio.NewReader(rc)
	for {
		s, err := r.ReadString('\n')
		if len(s) > 0 {
			log.Printf(s)
		}
		if err != nil {
			return
		}
	}
}

func (w *worker) execLoop() {
	for task := range w.tasks {
		task(w)
	}
}

func (w *worker) stop() {
	close(w.tasks)
	w.stdin.Close()
	logging.Infof("waiting for worker to stop")
	if err := w.cmd.Wait(); err != nil {
		logging.Errorf("failed to wait for worker to stop, err=%v", err)
	} else {
		logging.Infof("plot worker stopped")
	}
}

func (w *worker) startup() error {
	return w.runScript(startupPath)
}

func (w *worker) plotBars(symbol string, res time.Duration, end time.Time, count int, id string) (string, error) {
	filename := fmt.Sprintf("%s/%s.png", w.plotDir, id)
	code := fmt.Sprintf(plotBarsTemplate, symbol, int(res.Seconds()), end.Format("2006-01-02 15:04:05+00:00"), count, filename)
	_, err := io.WriteString(w.stdin, code)
	if err != nil {
		logging.Errorf("failed to write code, err=%v", err)
		return "", err
	}
	return bufio.NewReader(w.stdout).ReadString('\n')
}

func (w *worker) runScript(scriptPath string) error {
	code, err := ioutil.ReadFile(scriptPath)
	if err != nil {
		return err
	}
	_, err = w.stdin.Write(code)
	return err
}
