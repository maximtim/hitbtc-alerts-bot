package types

import (
	"time"
)

type Bar struct {
	Symbol      string
	Timestamp   time.Time
	Open        float64
	High        float64
	Low         float64
	Close       float64
	BaseVolume  float64
	QuoteVolume float64
}
