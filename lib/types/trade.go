package types

import "time"

const (
	SideBuy  = "buy"
	SideSell = "sell"
)

type Trade struct {
	Symbol    string
	Timestamp time.Time
	ID        int64
	Price     float64
	Qty       float64
	Side      string
}
