module sigbot/hitbtc_bot

go 1.17

require (
	github.com/google/uuid v1.3.0
	github.com/gorilla/websocket v1.4.2
	github.com/mattn/go-sqlite3 v1.14.9
	github.com/stretchr/testify v1.7.0
	golang.org/x/time v0.0.0-20211116232009-f0f3c7e86c11
)

require (
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)
